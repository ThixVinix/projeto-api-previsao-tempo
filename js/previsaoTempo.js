const colResultadoPrevisao =  document.querySelector("#idColResultadoPrevisao");
const accordionResultadoPrevisao = document.querySelector("#accordionResultadoPrevisao");
const btPrevisao = document.querySelector("#idBtPrevisao");
var containsListGroupItemAtivo = false;
var containsTabContentAtivo = false;

btPrevisao.addEventListener("click", function (event) {
    event.preventDefault();

    var formPrevisao = document.querySelector("#idFormPrevisao");

    var cidade = formPrevisao.nmInputCidade.value;
    var diasPosteriores = formPrevisao.idInputDiasPosteriores.value;

    if (cidade == null || cidade.trim() == "") {
        alert("Preencha o campo \"Cidade\".");
        return;
    }

    if (diasPosteriores == null || diasPosteriores.trim() == "") {
        alert("Preencha o campo \"Dias Posteriores\".");
        return;
    }

    if (isNaN(diasPosteriores)) {
        alert("O campo \"Dias Posteriores\" só aceita números.");
        return;
    }

    if (diasPosteriores < 1 || diasPosteriores > 3) {
        alert("Os valores permitidos para o preenchimento do campo \"Dias Posteriores\" são entre 1 à 3.");
        return;
    }

    while (accordionResultadoPrevisao.firstChild) {
        accordionResultadoPrevisao.removeChild(accordionResultadoPrevisao.firstChild);
    }

    obterPrevisao(cidade, diasPosteriores);

});

function obterPrevisao(cidade, diasPosteriores) {
    fetch("https://weatherapi-com.p.rapidapi.com/forecast.json?q=" + cidade + "&days=" + diasPosteriores, {
        "method": "GET",
        "headers": {
            "x-rapidapi-host": "weatherapi-com.p.rapidapi.com",
            "x-rapidapi-key": "9959e1e705msh9422956c4292a67p101207jsn3fe56966b6a1"
        }
    })
        .then(response => {
            return response.json();
        })
        .then(response => {
            console.log(response);
           
            if (response.forecast.forecastday.length == 0) {
                alert("Não foi possível realizar a previsão por conta do horário atual.\nTente mais tarde.");
            } else {
            exibirComponente(colResultadoPrevisao);
            for (let i = 0; i < response.forecast.forecastday.length; i++) {
                containsListGroupItemAtivo = false;
                containsTabContentAtivo = false;
                criarNovoAccordionItem(response.forecast.forecastday[i], i, response.location);
            }
        }
        })
        .catch(err => {
            console.error(err);
        });
}

function criarNovoAccordionItem(previsao, i, localizacao) {

    //Parte 1 - ACCORDION ITEM
    let accordionItem = document.createElement("div");
    accordionItem.setAttribute("class", "accordion-item");

    //Parte 2 - ACCORDION HEADER
    let accordionHeader = document.createElement("h2");
    accordionHeader.setAttribute("class", "accordion-header");
    accordionHeader.setAttribute("id", "heading" + (i + 1));

    //Parte 3 - ACCORDION BUTTON
    let accordionButton = document.createElement("button");
    accordionButton.setAttribute("class", "accordion-button collapsed");
    accordionButton.setAttribute("type", "button");
    accordionButton.setAttribute("data-bs-toggle", "collapse");
    accordionButton.setAttribute("data-bs-target", "#collapse" + (i + 1));
    accordionButton.setAttribute("aria-expanded", "false");
    accordionButton.setAttribute("aria-controls", "collapse" + (i + 1));
    alterarTextoComponente(accordionButton, "Dia " + previsao.date + " em " + localizacao.name + " (" + localizacao.region + "/" + localizacao.country + ")");


    //Parte 4 - ACCORDION COLLAPSE
    let accordionCollapse = document.createElement("div");
    accordionCollapse.setAttribute("id", "collapse" + (i + 1));
    accordionCollapse.setAttribute("class", "accordion-collapse collapse");
    accordionCollapse.setAttribute("aria-labelledby", "heading" + (i + 1));
    accordionCollapse.setAttribute("data-bs-parent", "#accordionResultadoPrevisao");

    //Parte 5 - ACCORDION BODY
    let accordionBody = document.createElement("div");
    accordionBody.setAttribute("class", "accordion-body");

    //Parte 6 - CONTEUDOS DO BODY

    // ********************* IMAGEM E LEGENDA *********************
    let figure = document.createElement("figure");
    let img = document.createElement("img");
    let figcaption = document.createElement("figcaption");

    img.setAttribute("src", "https://" + (previsao.day.condition.icon));
    img.setAttribute("class", "rounded mx-auto d-block");
    img.setAttribute("alt", "Imagem do Tempo");

    figcaption.setAttribute("class", "text-center");
    alterarTextoComponente(figcaption, previsao.day.condition.text);

    figure.appendChild(img);
    figure.appendChild(figcaption);

    // ********************* LIST GROUP *********************

    let colunaListGroup = document.createElement("div");
    let listGroup = document.createElement("div");
    let listGroupItem1 = document.createElement("a");
    let listGroupItem2 = document.createElement("a");
    let listGroupItem3 = document.createElement("a");
    let listGroupItem4 = document.createElement("a");
    let listGroupItem5 = document.createElement("a");
    let listGroupItem6 = document.createElement("a");
    let listGroupItem7 = document.createElement("a");
    let listGroupItem8 = document.createElement("a");
    let listGroupItem9 = document.createElement("a");
    let listGroupItem10 = document.createElement("a");
    let listGroupItem11 = document.createElement("a");
    let listGroupItem12 = document.createElement("a");
    let listGroupItem13 = document.createElement("a");

    colunaListGroup.setAttribute("class", "col-8 align-self-center");

    listGroup.setAttribute("class", "list-group");
    listGroup.setAttribute("id", "list-tab");
    listGroup.setAttribute("role", "tablist");

    configurarListGroupItem(listGroupItem1, previsao, "Average temperature (°C)", (i + 1));
    configurarListGroupItem(listGroupItem2, previsao, "Minimum temperature (°C)", (i + 2));
    configurarListGroupItem(listGroupItem3, previsao, "Maximum temperature (°C)", (i + 3));
    configurarListGroupItem(listGroupItem4, previsao, "Average temperature (°F)", (i + 4));
    configurarListGroupItem(listGroupItem5, previsao, "Minimum temperature (°F)", (i + 5));
    configurarListGroupItem(listGroupItem6, previsao, "Maximum temperature (°F)", (i + 6));
    configurarListGroupItem(listGroupItem7, previsao, "Daily chance of rain", (i + 7));
    configurarListGroupItem(listGroupItem8, previsao, "Daily chance of snow", (i + 8));
    configurarListGroupItem(listGroupItem9, previsao, "Moon phase", (i + 9));
    configurarListGroupItem(listGroupItem10, previsao, "Moonrise", (i + 10));
    configurarListGroupItem(listGroupItem11, previsao, "Moonset", (i + 11));
    configurarListGroupItem(listGroupItem12, previsao, "Sunrise", (i + 12));
    configurarListGroupItem(listGroupItem13, previsao, "Sunset", (i + 13));

    listGroup.appendChild(listGroupItem1);
    listGroup.appendChild(listGroupItem2);
    listGroup.appendChild(listGroupItem3);
    listGroup.appendChild(listGroupItem4);
    listGroup.appendChild(listGroupItem5);
    listGroup.appendChild(listGroupItem6);
    listGroup.appendChild(listGroupItem7);
    listGroup.appendChild(listGroupItem8);
    listGroup.appendChild(listGroupItem9);
    listGroup.appendChild(listGroupItem10);
    listGroup.appendChild(listGroupItem11);
    listGroup.appendChild(listGroupItem12);
    listGroup.appendChild(listGroupItem13);
    colunaListGroup.appendChild(listGroup);

    // ********************* TAB CONTENT *********************
    let colunaTabContent = document.createElement("div");
    let tabContent = document.createElement("div");
    let tabPane1 = document.createElement("div");
    let tabPane2 = document.createElement("div");
    let tabPane3 = document.createElement("div");
    let tabPane4 = document.createElement("div");
    let tabPane5 = document.createElement("div");
    let tabPane6 = document.createElement("div");
    let tabPane7 = document.createElement("div");
    let tabPane8 = document.createElement("div");
    let tabPane9 = document.createElement("div");
    let tabPane10 = document.createElement("div");
    let tabPane11 = document.createElement("div");
    let tabPane12 = document.createElement("div");
    let tabPane13 = document.createElement("div");

    colunaTabContent.setAttribute("class", "col-4 align-self-center");

    tabContent.setAttribute("class", "tab-content");
    tabContent.setAttribute("id", "nav-tabContent");

    configurarTabContent(tabPane1, previsao, (previsao.day.avgtemp_c + " °C"), (i + 1));
    configurarTabContent(tabPane2, previsao, (previsao.day.mintemp_c + " °C"), (i + 2));
    configurarTabContent(tabPane3, previsao, (previsao.day.maxtemp_c + " °C"), (i + 3));
    configurarTabContent(tabPane4, previsao, (previsao.day.avgtemp_f + " °F"), (i + 4));
    configurarTabContent(tabPane5, previsao, (previsao.day.mintemp_f + " °F"), (i + 5));
    configurarTabContent(tabPane6, previsao, (previsao.day.maxtemp_f + " °F"), (i + 6));
    configurarTabContent(tabPane7, previsao, (previsao.day.daily_chance_of_rain + " %"), (i + 7));
    configurarTabContent(tabPane8, previsao, (previsao.day.daily_chance_of_snow + " %"), (i + 8));
    configurarTabContent(tabPane9, previsao, (previsao.astro.moon_phase), (i + 9));
    configurarTabContent(tabPane10, previsao, (previsao.astro.moonrise), (i + 10));
    configurarTabContent(tabPane11, previsao, (previsao.astro.moonset), (i + 11));
    configurarTabContent(tabPane12, previsao, (previsao.astro.sunrise), (i + 12));
    configurarTabContent(tabPane13, previsao, (previsao.astro.sunset), (i + 13));

    tabContent.appendChild(tabPane1);
    tabContent.appendChild(tabPane2);
    tabContent.appendChild(tabPane3);
    tabContent.appendChild(tabPane4);
    tabContent.appendChild(tabPane5);
    tabContent.appendChild(tabPane6);
    tabContent.appendChild(tabPane7);
    tabContent.appendChild(tabPane8);
    tabContent.appendChild(tabPane9);
    tabContent.appendChild(tabPane10);
    tabContent.appendChild(tabPane11);
    tabContent.appendChild(tabPane12);
    tabContent.appendChild(tabPane13);
    colunaTabContent.appendChild(tabContent);

    //********************* LIST GROUP + TAB CONTENT *********************

    let linhaListGroup = document.createElement("div");
    linhaListGroup.setAttribute("class", "row");

    linhaListGroup.appendChild(colunaListGroup);
    linhaListGroup.appendChild(colunaTabContent);


    //********************* *********************

    accordionResultadoPrevisao.appendChild(accordionItem);

    accordionItem.appendChild(accordionHeader);
    accordionHeader.appendChild(accordionButton);

    accordionItem.appendChild(accordionCollapse);
    accordionCollapse.appendChild(accordionBody);

    accordionBody.appendChild(figure);
    accordionBody.appendChild(linhaListGroup);

}

function configurarListGroupItem(listGroupItem, previsao, descricaoConteudo, i) {
    if (containsListGroupItemAtivo == true) {
        listGroupItem.setAttribute("class", "list-group-item list-group-item-action");
    } else {
        containsListGroupItemAtivo = true;
        listGroupItem.setAttribute("class", "list-group-item list-group-item-action active");
    }
    listGroupItem.setAttribute("id", "list-" + (i) + "-" + (previsao.date) + "-list");
    listGroupItem.setAttribute("data-bs-toggle", "list");
    listGroupItem.setAttribute("href", "#list-" + (i) + "-" + (previsao.date));
    listGroupItem.setAttribute("role", "tab");
    listGroupItem.setAttribute("aria-controls", "list-" + (i) + "-" + (previsao.date));
    alterarTextoComponente(listGroupItem, descricaoConteudo);
}

function configurarTabContent(tabPane, previsao, conteudo, i) {
    if (containsTabContentAtivo == true) {
        tabPane.setAttribute("class", "tab-pane fade text-center");
    } else {
        containsTabContentAtivo = true;
        tabPane.setAttribute("class", "tab-pane fade text-center show active");
    }
    tabPane.setAttribute("id", "list-" + (i) + "-" + (previsao.date));
    tabPane.setAttribute("role", "tabpanel");
    tabPane.setAttribute("aria-labelledby", "list-" + (i) + "-" + (previsao.date) + "-list");
    alterarTextoComponente(tabPane, conteudo);
}

function aleatorizar(numMaximo) {
    return Math.floor(Math.random() * numMaximo);
}

function esconderComponente(componente) {
    componente.classList.add("visually-hidden");
}

function exibirComponente(componente) {
    componente.classList.remove("visually-hidden");
}

function desabilitarComponente(componente) {
    componente.disabled = true;
}

function habilitarComponente(componente) {
    componente.disabled = false;
}

function limparConteudoComponente(componente) {
    componente.value = "";
}

function tornarCampoObrigatorio(componente) {
    componente.required = true;
}

function tornarCampoOpcional(componente) {
    componente.required = false;
}

function alterarTextoComponente(componente, texto) {
    componente.innerHTML = texto
}


